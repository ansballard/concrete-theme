const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const { resolve } = require("path");
const pkg = require("./package.json");

module.exports = webpackEnv => {
    const env = Object.assign({}, process.env, webpackEnv);
    return ({
    	context: resolve(__dirname, "src"),
    	entry: "./index.jsx",
    	output: {
    		path: resolve(__dirname),
    		publicPath: "/",
    		filename: "bundle.js"
    	},
    	resolve: {
    	    extensions: [".jsx", ".js", ".json"],
    		modules: ["node_modules"]
    	},
    	module: {
    		loaders: [{
    				test: /\.jsx?$/,
    				exclude: /node_modules/,
    				use: "babel-loader"
    			}, {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: [{
                            loader: "css-loader",
                            options: {
                                importLoaders: 1
                            }
                        }, "postcss-loader"]
                    })
                }
    		]
    	},
    	plugins: ([
    		new webpack.NoEmitOnErrorsPlugin(),
    		new webpack.BannerPlugin({
    		   banner: (
`/**
 * Theme Name:       ${pkg.name}
 * Theme URI:        ${pkg.homepage}
 * Version:          ${pkg.version}
 * Description:      ${pkg.description}
 * Author:           ${pkg.author}
 * GitLab Theme URI: ${pkg.repository.url}
 */
`
                ),
    		   raw: true,
    		   test: /\.css$/
    		}),
    		new ExtractTextPlugin({
    			filename: "style.css",
    			allChunks: true
    		}),
    		new webpack.ProvidePlugin({
    		   Promise: resolve(__dirname, "node_modules", "es6-promise", "dist", "es6-promise.js"),
    		   fetch: [resolve(__dirname, "node_modules", "unfetch", "dist", "unfetch.es.js"), "default"]
    		}),
    		new webpack.DefinePlugin({
    			"process.env.NODE_ENV": JSON.stringify(env.NODE_ENV)
    		})
    	]).concat(env.NODE_ENV === "production" ? [
    		new webpack.optimize.UglifyJsPlugin({
    			output: {
    				comments: false
    			},
    			compress: true,
    			sourceMap: true,
    			warningsFilter: _ => false
    		})
    	] : []),
    	stats: { colors: true, children: false },
    	devtool: env.NODE_ENV === "production" ? "source-map" : "cheap-module-eval-source-map"
    });
}