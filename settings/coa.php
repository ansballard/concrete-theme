<?php
add_action('admin_init', 'concrete_admininit');

function concrete_admininit() {	

	register_setting(
		'general',
		'concrete_coabanner_url'
	);
	register_setting(
		'general',
		'concrete_coabanner_title'
	);
	register_setting(
		'general',
		'concrete_coabanner_content'
	);
	register_setting(
		'general',
		'concrete_coabanner_button'
	);
	register_setting(
		'general',
		'concrete_coabanner_form'
	);
	
	add_settings_section(  
        'concrete_coa_settings', // Section ID 
        'COA Banner Settings', // Section Title
        'concrete_settings_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

	add_settings_field(  
	    'concrete_coabanner_url',
	    'COA Banner URL',
	    'concrete_textbox_callback',
	    'general',
	    'concrete_coa_settings',
	    array( // The $args
            'concrete_coabanner_url'
        )
	);
	add_settings_field(  
	    'concrete_coabanner_title',
	    'COA Banner Title',
	    'concrete_textbox_callback',
	    'general',
	    'concrete_coa_settings',
	    array( // The $args
            'concrete_coabanner_title'
        )
	);
	add_settings_field(  
	    'concrete_coabanner_content',
	    'COA Banner Content',
	    'concrete_textbox_callback',
	    'general',
	    'concrete_coa_settings',
	    array( // The $args
            'concrete_coabanner_content'
        )
	);
	add_settings_field(  
	    'concrete_coabanner_button',
	    'COA Banner Button Text',
	    'concrete_textbox_callback',
	    'general',
	    'concrete_coa_settings',
	    array( // The $args
            'concrete_coabanner_button'
        )
	);
	add_settings_field(  
	    'concrete_coabanner_form',
	    'COA Banner Form URL',
	    'concrete_textbox_callback',
	    'general',
	    'concrete_coa_settings',
	    array( // The $args
            'concrete_coabanner_form'
        )
	);
	
	function concrete_settings_callback() { // Section Callback
        echo '';  
    }
 
	function concrete_textbox_callback($args) {  // Textbox Callback
        $option = get_option($args[0]);
        echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
    }
}
?>