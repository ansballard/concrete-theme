<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package concrete
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<span class="posted-on"><?php the_time('m/j/y');?></span> | <span class="byline"><?php the_author(); ?></span>
			<!-- <?php concrete_posted_on(); ?> -->
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			if( strpos( $post->post_content, '<!--more-->' ) && !is_single() ) {
				?><div class="read-more-wrapper">
						<img class="read-more-image" src="<?= wp_get_attachment_image_src(get_post_thumbnail_id(), "large")[0] ?>"/>
						<div class="read-more-content"><?php the_content(""); ?></div>
						<a class="read-more-link" href="<?=get_permalink()?>">Read More</a>
					</div><?php
			} else {
				the_content();
			}

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'concrete' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php concrete_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
