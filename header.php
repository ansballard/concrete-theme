<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package concrete
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'concrete' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<a href="/" id="header-image-wrapper">
		  	<h1><?= get_bloginfo('name'); ?></h1>
				<p><?= get_bloginfo( 'description') ?></p>
		  </a>
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	
	<?php
		if(get_option("concrete_coabanner_url") !== null && get_option("concrete_coabanner_url") !== "") { ?>

	<div id="coabanner-wrapper" class="coabanner-wrapper">
		<img id="coabanner-image" class="coabanner-image" src="<?= get_option("concrete_coabanner_url")?>"/>
		<div class="coabanner-content-wrapper">
			<h3 class="coabanner-title"><?= get_option("concrete_coabanner_title")?></h3>
			<p class="coabanner-content"><?= get_option("concrete_coabanner_content")?></p>
			<a href="<?= get_option("concrete_coabanner_form")?>" target="_blank"><button class="coabanner-button"><?= get_option("concrete_coabanner_button")?></button></a>
		</div>
	</div>
	
	<script> /* Sets a temporary height on the above div to prevent jank */
		var widthToHeight = 693 / 1200;
		var coabanner = document.getElementById("coabanner-wrapper");
		
		document.getElementById("coabanner-image").addEventListener("load", function() {
		    coabanner.style.height = "";
		});
		coabanner.style.height = (window.innerWidth >= 1200 ? 693 : window.innerWidth * widthToHeight) + "px";
	</script>
	
	<?php } ?>

	<div id="content" class="site-content">
		<!-- Wrap content, close in footer.php -->
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
