import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import npm from "rollup-plugin-node-resolve";
import json from "rollup-plugin-json";
import replace from "rollup-plugin-replace";
import uglify from "rollup-plugin-uglify";
import optimizejs from "optimize-js";

export default {
  entry: "src/js/index.jsx",
  dest: "bundle.js",
  sourceMap: process.env.NODE_ENV !== "production" ? true : undefined,
  sourceMapFile: process.env.NODE_ENV !== "production" ? "bundle.js.map" : undefined,
  plugins: [
    npm({
      browser: true
    }),
    json(),
    commonjs({
      include: "{node_modules|src}/**"
    }),
    babel({
      exclude: "node_modules/**"
    }),
    replace({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV)
    })
  ].concat(process.env.NODE_ENV === "production" ? [
    uglify({
      compress: true,
      mangle: true
    }),
    optimize()
  ] : []),
  format: "iife"
};

function optimize() {
  return {
    transformBundle(code) {
      return {
        code: optimizejs(code)
      };
    }
  };
}
