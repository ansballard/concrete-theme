module.exports = ({ file, options, env }) => ({
  plugins: {
    "postcss-import": {},
    "postcss-cssnext": {
        features: {
            autoprefixer: process.env.NODE_ENV !== "production"
        }
    },
    "cssnano": process.env.NODE_ENV !== "production" ? false : {}
  }
})