<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package concrete
 */

?>
			</main><!-- #main -->
			<?php get_sidebar(); ?>
		</div><!-- #primary -->

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<span class="copyright">copyright &copy; <?= date("Y"); ?></span> | 
			<a class="legal-stuff" href="/fun-legal-stuff" target="_blank">Privacy Policy</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
