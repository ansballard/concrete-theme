<?php
/* WIP */
class Mycustom_Widget extends WP_Widget {
  public function __construct() {

    parent::__construct(
      'thisisanew_widget',
      __( 'About Me', 'about_block' ),
      array(
        'classname' => 'thisisanew_widget',
        'description' => __( 'A simple about widget, with avatar and description', 'ansballard' )
      )
    );

    load_plugin_textdomain( 'thisisanewblock', false, basename( dirname( __FILE__ ) ) . '/languages' );

  }

  public function form( $instance ) {

    $title = esc_attr( $instance['title'] );
    $paragraph = esc_attr( $instance['paragraph'] );
    $imagelink = esc_attr( $instance['imagelink'] );
    $aboutlink = esc_attr( $instance['aboutlink'] );
    ?>

      <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('imagelink'); ?>"><?php _e('Image Link:'); ?></label>
        <br>
        <input class="widefat" type="url" value="<?php echo $imagelink; ?>" name="<?php echo $this->get_field_name('imagelink'); ?>" id="<?php echo $this->get_field_id('imagelink'); ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('aboutlink'); ?>"><?php _e('About Page Link:'); ?></label>
        <br>
        <input class="widefat" type="url" value="<?php echo $aboutlink; ?>" name="<?php echo $this->get_field_name('aboutlink'); ?>" id="<?php echo $this->get_field_id('aboutlink'); ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('paragraph'); ?>"><?php _e('Content'); ?></label>
        <br>
        <textarea class="widefat" rows="10" cols="16" name="<?php echo $this->get_field_name('paragraph'); ?>" id="<?php echo $this->get_field_id('paragraph'); ?>"><?php echo $paragraph; ?></textarea>
      </p>

    <?php
  }

  public function widget( $args, $instance ) {

    extract( $args );

    $title = apply_filters( 'widget_title', $instance['title'] );
    $imagelink = $instance['imagelink'];
    $aboutlink = $instance['aboutlink'];
    $paragraph = $instance['paragraph'];

    echo $before_widget;
    if ( $title ) {
      echo $before_title . $title . $after_title;
    }

    ?><div class="sidebar-about"><?php

    if ( ! empty($imagelink) ) {
      ?>
        <div class="sidebar-about-pic">
          <img src="<?php echo esc_attr($imagelink); ?>">
        </div>
      <?php
    }
    ?><div class="sidebar-about-text" style="text-align:center"><?php echo wpautop(esc_html__($paragraph));
    if ( ! empty($aboutlink) ) {
      ?>
        <a href="<?php echo esc_attr($aboutlink);?>">Learn More</a>
      <?php
    }
    ?></div></div><?php
    // if ( $paragraph ) {
    //   echo $before_paragraph . $paragraph . $after_paragraph;
    // }
    echo $after_widget;

  }

  public function update( $new_instance, $old_instance ) {

    $instance = array();

    $instance['title'] = (!empty( $new_instance['title'])) ? strip_tags( $new_instance['title'] ) : '';
    $instance['aboutlink'] = (!empty( $new_instance['aboutlink'])) ? strip_tags( $new_instance['aboutlink'] ) : '';
    $instance['imagelink'] = (!empty( $new_instance['imagelink'])) ? strip_tags( $new_instance['imagelink'] ) : '';
    $instance['paragraph'] = (!empty( $new_instance['paragraph'])) ? strip_tags( $new_instance['paragraph'] ) : '';

    return $instance;

  }
}

add_action( 'widgets_init', function(){
  register_widget( 'Mycustom_Widget' );
});

?>
