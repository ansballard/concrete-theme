<?php
/**
 * concrete functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package concrete
 */

if ( ! function_exists( 'concrete_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function concrete_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on concrete, use a find and replace
	 * to change 'concrete' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'concrete', get_template_directory() . '/languages' );

	// Add support for custom headers
	add_theme_support( 'custom-header', array(
		'width'         => 980,
		'height'        => 60,
		'default-image' => get_template_directory_uri() . '/images/header.jpg',
		'uploads'       => true,
	) );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'concrete' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'concrete_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'concrete_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function concrete_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'concrete_content_width', 640 );
}
add_action( 'after_setup_theme', 'concrete_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function concrete_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'concrete' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'concrete' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'concrete_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function concrete_scripts() {
	wp_enqueue_style( 'concrete-style', get_stylesheet_uri() );

	wp_enqueue_script( 'concrete-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	// wp_enqueue_script( 'bundle', get_template_directory_uri() . '/bundle.js', array(), '20170805', true );
	
	wp_enqueue_script('pinit', '//assets.pinterest.com/js/pinit.js', array(), '', true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'concrete_scripts' );

/**
 * SHOW FEATURED IMAGE THUMBNAIL ON POSTS PAGE
 */
add_filter('manage_posts_columns', 'add_thumbnail_column', 5);

function add_thumbnail_column($columns){
  $columns['new_post_thumb'] = __('Featured Image');
  return $columns;
}

add_action('manage_posts_custom_column', 'display_thumbnail_column', 5, 2);

function display_thumbnail_column($column_name, $post_id){
  switch($column_name){
    case 'new_post_thumb':
      $post_thumbnail_id = get_post_thumbnail_id($post_id);
      if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );
        echo '<img class="post-featured-image-thumbnail" width="100" src="' . $post_thumbnail_img[0] . '" />';
      }
      break;
  }
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Custom Widgets
 */
require('widgets/about_me.php');

/*
 * Custom Settings
 */
require("settings/coa.php");